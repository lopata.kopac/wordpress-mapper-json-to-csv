const fs = require('fs');
const settings = require('../settings.json') ;

console.log('Loading imput file');
var data = require(`../imput/${settings.imputFileName}`);

data = data[2].data;

console.log('Update settings data');
settings.columnMapping = Object.keys(data[0]).map((item, index) => {
    return {name: item, index: index + 1, newIndex: index + 1};
});

console.log('saving settings data');
fs.writeFileSync('./settings.json', JSON.stringify(settings, null, 2));

console.log('Done');
