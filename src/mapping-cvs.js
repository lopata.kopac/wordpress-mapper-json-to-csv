const fs = require('fs');
const settings = require('../settings.json');

console.log('Loading imput file');
var data = require(`../imput/${settings.imputFileName}`);
data = data[2].data;

console.log('Mapping data');
var result = data.map(tableRow => {
    var resultIten = [];

    for (let i = 0; i < settings.columnCount; i++) {
        resultIten[i] = null;
    }

    settings.columnMapping.forEach(settItem => {
        resultIten[settItem.newIndex - 1] = tableRow[settItem.name];
    });

    settings.columnSpliting.forEach(settItem => {
        var splited = tableRow[settItem.name].split(settItem.separateItem);
        settItem.to.forEach((toItem, toIndex) => {
            if (splited[toIndex])
                resultIten[toItem.index - 1] = splited[toIndex];
            else
                resultIten[toItem.index - 1] = '';
        })
    });

    settings.columnReplace.forEach(settItem => {
        resultIten[settItem.index - 1] = settItem.replaceString;
    });

    return resultIten;
});

result = result.map(resulitem => {
    return resulitem.map(collumItem =>{
        if (collumItem === null | collumItem == undefined | collumItem === 'null')
            return 'NULL';
        else
            return collumItem;
    });
})

console.log('Saving to CSV file');
fs.writeFileSync(`./output/${settings.outputFileName}`, result.map(v => '\"' + v.join('\",\"') ).join('\"\n'));

console.log('Done');
