# wordpress-mapper-json-to-csv

Wordpress Mapper

First set you imput and output file in output and imput folder in settings.json.

    "imputFileName": "test.json",
    "outputFileName": "test.csv",

Run for initial columns

`npm run init-settings`

    "columnMapping": [
        {
          "name": "id",
          "index": 1,
          "newIndex": 1
        },
        {
          "name": "name",
          "index": 2,
          "newIndex": 2
        },
        {
          "name": "email",
          "index": 3,
          "newIndex": 3
        }
    ],

You can change column index to new index you want.
    
In columnSpiting you can slpit column by space of odher serapeItem to odher Column.
    
    "columnSpliting": [
        {
          "name": "name",
          "index": 3,
          "separateItem": " ",
          "to": [
            {
              "name": "first_name",
              "index": 4
            },
            {
              "name": "last_name",
              "index": 5
            }
          ]
        }
    ]
  
If you need Create Column witch spefific content in all rou you can use columnReplace.

    "columnReplace": [
        {
          "name": "created",
          "index": 6,
          "replaceString": "2020-04-23 00:00:00"
        }
    ]

For build you csv file run.

`npm run build-csv`